﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Yunish
{
    public partial class Login : Form
    {
        Entities x;
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            x = new Entities();
            if(textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("Username and Password Required");
            }
            else if (comboBox1.Text == "")
            {
                MessageBox.Show("Select Role");
            }
            else
            {
                var a = from s in x.usertbl where s.username == textBox1.Text && s.pass == textBox2.Text && s.role == comboBox1.Text select s;
                if (a.SingleOrDefault() == null)
                {
                    MessageBox.Show("Incorrect Username Or Password Please Try agin");

                }

                else
                {
                    if (comboBox1.Text == "Admin" || comboBox1.Text == "admin")
                    {
                        Admin admin = new Admin();
                        admin.Show();
                        this.Hide();

                    }
                    else if (comboBox1.Text == "Tester" || comboBox1.Text == "tester")
                    {
                        Tester tester = new Tester();
                        tester.Show();
                        this.Hide();

                    }
                    else
                    {
                        session.session_user = textBox1.Text;
                        Debugger debugger = new Debugger();
                        debugger.Show();
                        this.Hide();

                    }
                }
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Yunish
{
    public partial class Debugger : Form
    {
        Entities x;
        public Debugger()
        {
            InitializeComponent();
        }

        private void Debugger_Load_1(object sender, EventArgs e)
        {
            x = new Entities();
            textBox1.ReadOnly = true;
            textBox2.ReadOnly = true;
            textBox3.ReadOnly = true;
            
            lbl.Hide();
            var session_user = session.session_user;
            assigntblBindingSource2.DataSource = x.assigntbl.ToList();
            // int rowindex = dataViewAssignBug.CurrentCell.RowIndex;
            //labelBugId.Text = dataGridViewBug.Rows[rowindex].Cells[3].Value.ToString(); 
            //var sql= from a in x.usertbl where 

            var sql = from a in assigntblBindingSource2.DataSource as List<assigntbl>
                      where a.username == session_user
                      select a;
            if (sql.FirstOrDefault() != null)
            {
                MessageBox.Show("bugs not assigned");
            }
        }

        private void buttonSubmit_Click_1(object sender, EventArgs e)
        {
            var session_user = session.session_user;

            int curRow = dataGridView1.CurrentCell.RowIndex;
            //lbl.Text = dataGridView1.Rows[curRow].Cells[5].Value.ToString();
            //textBox1.Text = dataGridView1.Rows[curRow].Cells[0].Value.ToString();

            var sql = from a in x.assigntbl where a.bugid == textBox1.Text select a;
            var sql2 = from a in x.bugtbl where a.id.ToString() == textBox1.Text select a;
            if (!Solved.Checked && !UnSolved.Checked)
            {
                MessageBox.Show("click any check box please");

            }
            else if (
                textBox1.Text == "" ||
                textBox2.Text == "" ||
                textBox3.Text == "" ||
                richTextBox1.Text == ""

                )
            {
                MessageBox.Show("Enter all the field.");

            }
            else if (Solved.Checked && !UnSolved.Checked)
            {
                foreach (assigntbl f in sql)
                {
                    f.syntax = richTextBox1.Text;
                    f.status = "solved";

                }
                x.SaveChanges();
                MessageBox.Show("submitted");
            }
            else if (!Solved.Checked && UnSolved.Checked)
            {
                foreach (assigntbl f in sql)
                {
                    f.syntax = richTextBox1.Text;
                    f.status = "unsolved";

                }
                x.SaveChanges();
                MessageBox.Show("submitted");
            }

            else if (Solved.Checked && UnSolved.Checked)
            {
                MessageBox.Show("Select Only one Option");

            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int curRow = dataGridView1.CurrentCell.RowIndex;
            textBox1.Text = dataGridView1.Rows[curRow].Cells[5].Value.ToString();
            textBox2.Text = dataGridView1.Rows[curRow].Cells[1].Value.ToString();
            textBox3.Text = dataGridView1.Rows[curRow].Cells[2].Value.ToString();
            richTextBox1.Text = dataGridView1.Rows[curRow].Cells[7].Value.ToString();
        }
    }
}

﻿namespace ASE_Yunish
{
    partial class Tester
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxBugId = new System.Windows.Forms.TextBox();
            this.textBoxSoftName = new System.Windows.Forms.TextBox();
            this.textBoxBugDesc = new System.Windows.Forms.TextBox();
            this.rtSyntax = new System.Windows.Forms.RichTextBox();
            this.buttonSubmit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Bug Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(47, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Software";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(47, 125);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bug Description";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(47, 162);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Syntax";
            // 
            // textBoxBugId
            // 
            this.textBoxBugId.Location = new System.Drawing.Point(135, 40);
            this.textBoxBugId.Name = "textBoxBugId";
            this.textBoxBugId.ReadOnly = true;
            this.textBoxBugId.Size = new System.Drawing.Size(223, 20);
            this.textBoxBugId.TabIndex = 4;
            // 
            // textBoxSoftName
            // 
            this.textBoxSoftName.Location = new System.Drawing.Point(135, 80);
            this.textBoxSoftName.Name = "textBoxSoftName";
            this.textBoxSoftName.Size = new System.Drawing.Size(223, 20);
            this.textBoxSoftName.TabIndex = 0;
            // 
            // textBoxBugDesc
            // 
            this.textBoxBugDesc.Location = new System.Drawing.Point(135, 125);
            this.textBoxBugDesc.Name = "textBoxBugDesc";
            this.textBoxBugDesc.Size = new System.Drawing.Size(223, 20);
            this.textBoxBugDesc.TabIndex = 1;
            // 
            // rtSyntax
            // 
            this.rtSyntax.Location = new System.Drawing.Point(135, 162);
            this.rtSyntax.Name = "rtSyntax";
            this.rtSyntax.Size = new System.Drawing.Size(454, 201);
            this.rtSyntax.TabIndex = 2;
            this.rtSyntax.Text = "";
            // 
            // buttonSubmit
            // 
            this.buttonSubmit.Location = new System.Drawing.Point(473, 369);
            this.buttonSubmit.Name = "buttonSubmit";
            this.buttonSubmit.Size = new System.Drawing.Size(75, 23);
            this.buttonSubmit.TabIndex = 3;
            this.buttonSubmit.Text = "submit";
            this.buttonSubmit.UseVisualStyleBackColor = true;
            this.buttonSubmit.Click += new System.EventHandler(this.button1_Click);
            // 
            // Tester
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(601, 404);
            this.Controls.Add(this.buttonSubmit);
            this.Controls.Add(this.rtSyntax);
            this.Controls.Add(this.textBoxBugDesc);
            this.Controls.Add(this.textBoxSoftName);
            this.Controls.Add(this.textBoxBugId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Tester";
            this.Text = "Tester";
            this.Load += new System.EventHandler(this.Tester_Load_1);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxBugId;
        private System.Windows.Forms.TextBox textBoxSoftName;
        private System.Windows.Forms.TextBox textBoxBugDesc;
        private System.Windows.Forms.RichTextBox rtSyntax;
        private System.Windows.Forms.Button buttonSubmit;
    }
}
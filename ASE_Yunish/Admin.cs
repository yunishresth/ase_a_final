﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Yunish
{
    public partial class Admin : Form
    {
        Entities x;
        public Admin()
        {
            InitializeComponent();
        }



        private void Admin_Load(object sender, EventArgs e)
        {

            x = new Entities();
            panel.Enabled = false;
            panel1.Enabled = false;

            Update.Hide();
            bugtblBindingSource.DataSource = x.bugtbl.ToList();
            assigntblBindingSource.DataSource = x.assigntbl.ToList();


        }
        private void enableUpdate_Click(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            var query = from s in x.assigntbl where s.bugid == label2.Text select s;
            if (query.SingleOrDefault() == null)
            {
                Update.Hide();
                buttonAssign.Show();


            }
            else
            {
                Update.Show();
                buttonAssign.Hide();


            }
        }

        private void Update_Click(object sender, EventArgs e)
        {

            var q = from a in x.assigntbl where a.bugid == label2.Text && a.fullname == comboBox1.Text select a;
            foreach (assigntbl a in q)
            {
                a.softname = textBox1.Text;
                a.bugdesc = textBox2.Text;
                a.syntax = richTextBox1.Text;


            }
            try
            {
                x.SaveChanges();
                Update.Hide();
                MessageBox.Show("updated");
                panel1.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonAssign_Click(object sender, EventArgs e)
        {

            var debugger = comboBox1.Text;
            var q = from s in x.assigntbl where s.fullname == debugger && s.bugid == label2.Text select s;
            if (q.SingleOrDefault() != null)
            {
                MessageBox.Show("This bug is already assigned to this debugger. If you still want to assign, then enable update and click update after the changes");
                buttonAssign.Hide();
                Update.Show();


            }
            else
            {

                var a = new assigntbl()
                {
                    bugid = label2.Text,
                    softname = textBox1.Text,
                    bugdesc = textBox2.Text,
                    syntax = richTextBox1.Text,
                    fullname = comboBox1.Text,
                    status = "Assigned"
                };
                x.assigntbl.Add(a);
                x.SaveChanges();
                MessageBox.Show("Bug has been assigned");
                buttonAssign.Hide();
            }
        }



        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Assign")
            {
                panel.Enabled = true;
                int rowindex = dataGridView1.CurrentCell.RowIndex;
                label2.Text = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
                textBox1.Text = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                richTextBox1.Text = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();

                var a = from s in x.usertbl where s.role == "Debugger" select s;
                comboBox1.DataSource = a.ToList();
                comboBox1.DisplayMember = "fullname";
                comboBox1.ValueMember = "id";
                panel1.Enabled = false;
                buttonAssign.Show();
                Update.Hide();

            }
        }

        private void Admin_Load_1(object sender, EventArgs e)
        {
            x = new Entities();
            panel.Enabled = false;
            panel1.Enabled = false;

            Update.Hide();
            bugtblBindingSource2.DataSource = x.bugtbl.ToList();
            assigntblBindingSource2.DataSource = x.assigntbl.ToList();
        }

        private void enableUpdate_Click_1(object sender, EventArgs e)
        {
            panel1.Enabled = true;
            var query = from s in x.assigntbl where s.bugid == label2.Text select s;
            if (query.SingleOrDefault() == null)
            {
                Update.Hide();
                buttonAssign.Show();


            }
            else
            {
                Update.Show();
                buttonAssign.Hide();


            }
        }

        private void Update_Click_1(object sender, EventArgs e)
        {
            var q = from a in x.assigntbl where a.bugid == label2.Text && a.fullname == comboBox1.Text select a;
            foreach (assigntbl a in q)
            {
                a.softname = textBox1.Text;
                a.bugdesc = textBox2.Text;
                a.syntax = richTextBox1.Text;


            }
            try
            {
                x.SaveChanges();
                Update.Hide();
                MessageBox.Show("updated");
                panel1.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonAssign_Click_1(object sender, EventArgs e)
        {
            var debugger = comboBox1.Text;
            var q = from s in x.assigntbl where s.fullname == debugger && s.bugid == label2.Text select s;
            if (q.SingleOrDefault() != null)
            {
                MessageBox.Show("This bug is already assigned to this debugger. If you still want to assign, then enable update and click update after the changes");
                buttonAssign.Hide();
                Update.Show();


            }
            else
            {

                var a = new assigntbl()
                {
                    bugid = label2.Text,
                    softname = textBox1.Text,
                    bugdesc = textBox2.Text,
                    syntax = richTextBox1.Text,
                    fullname = comboBox1.Text,
                    status = "Assigned"
                };
                x.assigntbl.Add(a);
                x.SaveChanges();
                MessageBox.Show("Bug has been assigned");
                buttonAssign.Hide();
            }
        }

        private void dataGridView1_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex].Name == "Assign")
            {
                panel.Enabled = true;
                int rowindex = dataGridView1.CurrentCell.RowIndex;
                label2.Text = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
                textBox1.Text = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
                textBox2.Text = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
                richTextBox1.Text = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();

                var a = from s in x.usertbl where s.role == "Debugger" select s;
                comboBox1.DataSource = a.ToList();
                comboBox1.DisplayMember = "fullname";
                comboBox1.ValueMember = "id";
                panel1.Enabled = false;
                buttonAssign.Show();
                Update.Hide();

            }
        }
    }
}

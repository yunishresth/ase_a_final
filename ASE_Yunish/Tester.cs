﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ASE_Yunish
{
    public partial class Tester : Form
    {
        Entities x;
        public Tester()
        {
            InitializeComponent();
        }

        private void Tester_Load_1(object sender, EventArgs e)
        {
            textBoxBugId.ReadOnly = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            x = new Entities();
            if (
                textBoxSoftName.Text == "" ||
                textBoxBugDesc.Text == "" ||
                rtSyntax.Text == ""
                )
            {
                MessageBox.Show("Please enter all field");

            }
            else
            {
                try
                {
                    var q = new bugtbl()
                    {
                        bugdesc = textBoxBugDesc.Text,
                        softname = textBoxSoftName.Text,
                        syntax = rtSyntax.Text,
                        status = ""

                    };
                    x.bugtbl.Add(q);
                    x.SaveChanges();
                    MessageBox.Show("Bug has been submitted.");
                    textBoxSoftName.Text = "";
                    textBoxBugDesc.Text = "";
                    rtSyntax.Text = "";
                }


                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
